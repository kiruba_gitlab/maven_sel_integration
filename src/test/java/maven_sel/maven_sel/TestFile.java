package maven_sel.maven_sel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestFile {
	
	//static String driver_file="src/test/java/maven_sel/mydrivers/chromedriver_win32_B39/chromedriver.exe";
	static String driver_file="/tmp/chromedriver.exe";
	
	@Test
	public void test1() throws InterruptedException {
		System.setProperty("webdriver.gecko.driver", driver_file);
		WebDriver driver = new FirefoxDriver();
		
		driver.get("http://34.243.169.174:3001/");
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='login']")).sendKeys("edureka");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Admin");
		driver.findElement(By.xpath("//input[@name='click']")).click();
		Thread.sleep(1000);
		String output=driver.findElement(By.xpath("//div[@name='hello']")).getText();
		System.out.println(output);
		driver.quit();
	}
	
	@Test
	public void test2() throws InterruptedException {
		System.setProperty("webdriver.gecko.driver", driver_file);
		WebDriver driver = new FirefoxDriver();
		
		driver.get("http://34.243.169.174:3001/");
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='login']")).sendKeys("edu");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Admin");
		driver.findElement(By.xpath("//input[@name='click']")).click();
		Thread.sleep(1000);
		String output=driver.findElement(By.xpath("//div[@name='hello']")).getText();
		System.out.println(output);
		driver.quit();
	}
	
	@Test
	public static void main() throws InterruptedException{	
		
		System.setProperty("webdriver.gecko.driver", driver_file);
		WebDriver driver = new FirefoxDriver();
		
		driver.get("http://34.243.169.174:3001/");
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='login']")).sendKeys("Admin");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Admin");
		driver.findElement(By.xpath("//input[@name='click']")).click();
		Thread.sleep(1000);
		String output=driver.findElement(By.xpath("//div[@name='hello']")).getText();
		System.out.println(output);
		driver.quit();
	
	}
	
	
	
}
