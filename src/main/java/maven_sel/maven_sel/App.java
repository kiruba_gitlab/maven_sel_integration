package maven_sel.maven_sel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Hello world!
 *
 */
public class App 
{
	static WebDriver driver;
	
	public String testcase(String username,String pass) throws InterruptedException {
		String output=null;
		driver.findElement(By.xpath("//input[@name='login']")).sendKeys(username);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(pass);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@name='click']")).click();
		Thread.sleep(1000);
		output=driver.findElement(By.xpath("//div[@name='hello']")).getText();
		return output;
	}
	
    public static void main( String[] args ) throws InterruptedException
    {
    	   		
    		System.setProperty("webdriver.gecko.driver", "src\\test\\java\\maven_sel\\mydrivers\\chromedriver_win32_B39\\chromedriver.exe");
    		driver = new FirefoxDriver();
    		App obj = new App();
    		driver.get("http://34.243.169.174:3001/");
    		Thread.sleep(5000);
    		
    		System.out.println(obj.testcase("Admin","pass"));
    		Thread.sleep(1000);
    		System.out.println(obj.testcase("Admin1","pass2"));
    		Thread.sleep(1000);
    		System.out.println(obj.testcase("edureka","passs"));
    		Thread.sleep(1000);
    		System.out.println(obj.testcase("Adm","passs12ss"));
    		Thread.sleep(1000);
    		System.out.println(obj.testcase("edureka","edureka"));
    		Thread.sleep(1000);
    		System.out.println(obj.testcase("Admina","passs#"));
    		Thread.sleep(1000);
    		System.out.print("Test Ends");
    		driver.quit();
    	
      	
    }
}
